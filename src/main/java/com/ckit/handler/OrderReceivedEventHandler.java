package com.ckit.handler;

import com.ckit.event.OrderReceivedEvent;
import com.ckit.framework.Handler;
import com.ckit.ShelfManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OrderReceivedEventHandler implements Handler<OrderReceivedEvent> {
    private static final Logger LOG = LogManager.getRootLogger();

    private final ShelfManager shelfManager;

    public OrderReceivedEventHandler(ShelfManager shelfManager) {
        this.shelfManager = shelfManager;
        LOG.info("ctor {}", getClass().getSimpleName());
    }

    @Override
    public void onEvent(OrderReceivedEvent event) {
        shelfManager.handleOrderReceived(event.getOrder());
    }


}

package com.ckit.event;

/**
 * The {@link CourierArrivedEvent} should be fired when courier arrives at the kitchen.
 *
 * On courier arrival event, courier will instantly pickup, and instantly deliver it.
 */
public class CourierArrivedEvent extends AbstractEvent {

    private final String id;

    public CourierArrivedEvent(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }
}

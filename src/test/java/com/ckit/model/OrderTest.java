package com.ckit.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class OrderTest {

    @Test
    void positiveValueNotWasted() {
        Order order = new Order("a0", "orange", "cold", 100, 1.0f, LocalDateTime.now(), Shelf.Cold.name());
        assertThat(order.getValue()).isEqualTo(1.0d);
        assertThat(order.isWasted()).isFalse();
    }

    @Test
    void wastedZero() {
        Order order = new Order("a0", "orange", "cold", 100, 1.0f, LocalDateTime.now().plusSeconds(-100),
            Shelf.Cold.name());
        assertThat(order.getValue()).isEqualTo(0.0d);
        assertThat(order.isWasted()).isTrue();
    }

    @Test
    void wastedNegative() {
        Order order = new Order("a0", "orange", "cold", 100, 1.0f, LocalDateTime.now().plusSeconds(-101),
            Shelf.Cold.name());
        assertThat(order.getValue()).isEqualTo(-0.01d);
        assertThat(order.isWasted()).isTrue();
    }

    @Test
    void copyWithShelf() {
        Order order = getUnExpiredColdOrder();

        Order copy = Order.copyWithShelf(order, Shelf.Overflow.name());

        assertThat(copy.getShelf()).isNotEqualTo(order.getShelf());

        // All fields are identical, except for shelf.
        assertThat(copy.getShelf()).isEqualTo(Shelf.Overflow.name());
        assertThat(copy.getId()).isEqualTo(order.getId());
        assertThat(copy.getName()).isEqualTo(order.getName());
        assertThat(copy.getTemp()).isEqualTo(order.getTemp());
        assertThat(copy.getShelfLife()).isEqualTo(order.getShelfLife());
        assertThat(copy.getDecayRate()).isEqualTo(order.getDecayRate());
        assertThat(copy.getTime()).isEqualTo(order.getTime());
    }

    @Test
    void testHashcodeEquals() {
        // is same
        Order order1 = new Order("a0", "orange", "cold", 200, 1.0f);
        Order order2 = new Order("a0", "salad", "hot", 200, 0.3f);
        assertThat(order1.hashCode()).isEqualTo(order2.hashCode());
        assertThat(order1.equals(order2)).isTrue();

        // is different
        Order order3 = new Order("a3", "orange", "cold", 200, 1.0f);
        assertThat(order1.hashCode()).isNotEqualTo(order3.hashCode());
        assertThat(order1.equals(order3)).isFalse();
    }

    private Order getUnExpiredColdOrder() {
        return new Order("a0", "orange", "cold", 100, 1.0f, LocalDateTime.now(), Shelf.Cold.name());
    }

}

package com.ckit;

import com.ckit.event.OrderReceivedEvent;
import com.ckit.framework.EventDispatcher;
import com.ckit.model.Order;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import java.util.Iterator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * alternate design could be to use queue:
 * BlockingQueue<String> blockingQueue = new LinkedBlockingDeque<>();
 */
public class OrderTaskProducer implements Runnable {
    private static final Logger LOG = LogManager.getRootLogger();
    private final Iterator<JsonElement> iter;
    private final EventDispatcher dispatcher;
    private final ShelfManager shelfManager;
    private final Application application;

    public OrderTaskProducer(EventDispatcher dispatcher,
        ShelfManager shelfManager,
        Application application,
        Iterator<JsonElement> iter)
    {
        this.dispatcher = dispatcher;
        this.shelfManager = shelfManager;
        this.application = application;
        this.iter = iter;
        LOG.info("ctor {}", getClass().getSimpleName());
    }

    @Override
    public void run() {
        if (!iter.hasNext()) {
            if (shelfManager.size() == 0) {
                LOG.info("{} no remaining orders, so shutdown. ", getClass().getSimpleName());
                application.executorShutdown();
            }
            return;
        }
        JsonElement elem = this.iter.next();
        JsonObject json = elem.getAsJsonObject();

        Order order = new Order(
            json.get("id").getAsString(),
            json.get("name").getAsString(),
            json.get("temp").getAsString(),
            json.get("shelfLife").getAsInt(),
            json.get("decayRate").getAsFloat());

        dispatcher.dispatch(new OrderReceivedEvent(order));

        // can't use fromJson since CTOR isn't called and time field isn't set.
        // gson doesn't call the ctor, so initialize the time of order.
        //        Order order = gson.fromJson(elem, Order.class);
    }
}

package com.ckit.event;

import com.ckit.model.Order;

/**
 * The {@link OrderPickedUpEvent} should should be dispatched whenever an order is
 * picked up by courier.
 */
@SuppressWarnings("ALL")
public class OrderPickedUpEvent extends AbstractEvent {

    private final Order order;

    public OrderPickedUpEvent(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }
}

package com.ckit;

import com.ckit.event.CourierArrivedEvent;
import com.ckit.event.DispatchCourierEvent;
import com.ckit.event.OrderDiscardedEvent;
import com.ckit.event.OrderPickedUpEvent;
import com.ckit.event.OrderReceivedEvent;
import com.ckit.framework.Event;
import com.ckit.framework.EventDispatcher;
import com.ckit.handler.CourierArrivedEventHandler;
import com.ckit.handler.DispatchCourierEventHandler;
import com.ckit.handler.OrderDiscardedEventHandler;
import com.ckit.handler.OrderPickedUpEventHandler;
import com.ckit.handler.OrderReceivedEventHandler;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;

/*
8/7/20 the feedback from the debrief of the challenge:
   1. readme didn't show how to start the app. but that was not in the rubric. this is a
   standard spring app.
   2. poor data structure and fundamentals.
   3. good architecture, and completeness.
 */
/**
 * An event-driven architecture (EDA) is a framework that orchestrates behavior around the
 * production, detection and consumption of events as well as the responses they evoke.
 *
 * <p>I use an {@link EventDispatcher} to link {@link Event} objects to
 * their respective handlers. once an event is dispatched, it's respective handler is invoked.

 * To use @Value, use static PropertySourcesPlaceholderConfigurer
 * https://mkyong.com/spring/spring-is-not-working-in-value/

 Created on 7/29/20
 */
@Configuration
@ComponentScan({"com.ckit", "com.ckit.framework", "com.ckit.handler"})
@PropertySource("classpath:config.properties")
public class Application {
    private static final Logger LOG = LogManager.getRootLogger();
    static final String JSON_FOOD_ORDER = "food_order.json";

    static AnnotationConfigApplicationContext ctx;
    static Properties appProps;

    static EventDispatcher dispatcher;
    static ShelfManager shelfManager;
    static OrderTaskProducer orderTaskProducer;
    static OrderReceivedEventHandler orderReceivedEventHandler;
    static CourierArrivedEventHandler courierArrivedEventHandler;
    static OrderPickedUpEventHandler orderPickedUpEventHandler;
    static DispatchCourierEventHandler dispatchCourierEventHandler;
    static OrderDiscardedEventHandler orderDiscardedEventHandler;

    final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();

    public static void main(String[] args) {
        ctx = new AnnotationConfigApplicationContext(Application.class);
        appProps = ApplicationUtil.getProps();
        int randomRangeStart = Integer.parseInt(appProps.getProperty("random-range-start", "2"));
        int randomRangeEnd = Integer.parseInt(appProps.getProperty("random-range-end", "6"));

        // example:  Could get json file from cmd args.
        try (InputStream inputStream = Application.class.getClassLoader().getResourceAsStream(
            JSON_FOOD_ORDER)) {
            assert inputStream != null;
            Application driver = ctx.getBean(Application.class);
            driver.startWithStream(inputStream, randomRangeStart, randomRangeEnd);
        } catch (IOException e) {
            LOG.error(e.getMessage());
        }
    }

    public void executorShutdown() {
        LOG.info("{} shutdown", Application.class.getSimpleName());
        executor.shutdown();
        System.exit(1);
    }

    void startWithStream(InputStream inputStream, int randomRangeStart, int randomRangeEnd) {
        Iterator<JsonElement> iter = getJsonElementIterator(inputStream);
        instantiateHandlers(iter, randomRangeStart, randomRangeEnd);
        registerHandlers();
        processStream();
    }

    Iterator<JsonElement> getJsonElementIterator(InputStream inputStream) {
        JsonElement json = JsonParser.parseReader(new InputStreamReader(inputStream));
        return json.getAsJsonArray().iterator();
    }

    /**
     Once the {@link EventDispatcher} is initialised, handlers related to specific events have to
     be made known to the dispatcher by registering them. In this case the OrderReceivedEvent
     is bound to the OrderReceivedEventHandler,. The dispatcher can now be
     called to dispatch specific events.
     */
    void instantiateHandlers(Iterator<JsonElement> iter, int randomRangeStart, int randomRangeEnd) {

        dispatcher = EventDispatcher.getInstance();
        shelfManager = ShelfManager.init(dispatcher);
        orderReceivedEventHandler = new OrderReceivedEventHandler(shelfManager);
        courierArrivedEventHandler = new CourierArrivedEventHandler(shelfManager);
        orderPickedUpEventHandler = new OrderPickedUpEventHandler();
        dispatchCourierEventHandler = new DispatchCourierEventHandler(dispatcher, randomRangeStart,
            randomRangeEnd);
        orderDiscardedEventHandler = new OrderDiscardedEventHandler();
        orderTaskProducer = new OrderTaskProducer(dispatcher, shelfManager, this, iter);
    }

    void registerHandlers() {
        dispatcher.registerHandler(OrderReceivedEvent.class, orderReceivedEventHandler);
        dispatcher.registerHandler(DispatchCourierEvent.class, dispatchCourierEventHandler);
        dispatcher.registerHandler(CourierArrivedEvent.class, courierArrivedEventHandler);
        dispatcher.registerHandler(OrderPickedUpEvent.class, orderPickedUpEventHandler);
        dispatcher.registerHandler(OrderDiscardedEvent.class, orderDiscardedEventHandler);
    }

    void processStream() {
        long periodMilli = getPeriodMilli();
        executor.scheduleAtFixedRate(orderTaskProducer, 0L, periodMilli, TimeUnit.MILLISECONDS);
    }

    long getPeriodMilli() {
        int ordersPerSecond = Integer.parseInt(appProps.getProperty("orders-per-second", "2"));
        long periodMilli = (1000L / ordersPerSecond);
        LOG.info("ordersPerSecond: " + ordersPerSecond + "   periodMilli duration: " + periodMilli);
        return periodMilli;
    }

}

package com.ckit.model;

import com.google.gson.annotations.Expose;
import java.time.LocalDateTime;
import javax.annotation.Nonnull;
import org.apache.commons.lang3.StringUtils;

/**
 * Immutable class.
 */
public class Order {
    public static final int ID_SHORT_END_INDEX = 4;
    // Looks like a UUID, but we'll leave it as String to simplify
    private final String id;
    private final String name;
    private final Temperature temp;
    private final int shelfLife;
    private final float decayRate;
    private final String shelf;

    //  LocalDateTime : date and time w/o timezone in ISO-8601 format (yyyy-MM-ddTHH:mm:ss.SSS)
    // 'transient' stops this warning: "Illegal reflective access by com.google.gson.internal.reflect.UnsafeReflectionAccessor"
    @Expose(serialize = false, deserialize = false)
    final transient LocalDateTime time;

    /**
     * Allow order start time to be passed in; in tests, we simulate different times.
     */
    public Order(@Nonnull String id, String name, String temp, int shelfLife, float decayRate,
        LocalDateTime time, String shelf) {
        this.id = id;
        this.name = name;
        this.temp = Temperature.valueOf(temp);
        this.shelfLife = shelfLife;
        this.decayRate = decayRate;
        this.time = time;
        this.shelf = shelf;
        validateShelf(shelf, temp);
    }

    // This CTOR without time, shelf.
    public Order(@Nonnull String id, String name, String temp, int shelfLife, float decayRate) {
        this(id, name, temp, shelfLife, decayRate, LocalDateTime.now(), null);
    }

    public static Order copyWithShelf(Order order, String shelf) {
        return new Order(order.getId(), order.getName(), order.getTemp().name(),
            order.getShelfLife(), order.getDecayRate(), order.getTime(), shelf);
    }

    public double getValue() {
        return OrderValueCalc.orderValue(this, LocalDateTime.now());
    }

    public boolean isWasted() {
        return getValue() <= 0;
    }
    public String getId() {
        return shortenId(id);
    }

    void validateShelf(@Nonnull String shelfString, @Nonnull String tempString) {
        if (StringUtils.isBlank(shelfString)
            || StringUtils.equals("Overflow", shelfString)
            || StringUtils.equalsIgnoreCase(shelfString, tempString)) {
            return;
        }
        throw new IllegalArgumentException("id " + id + " shelf " + shelfString
            + " does not match temperature " + tempString);
    }

    String shortenId(String id) {
        return id.substring(0, Math.min(id.length(),ID_SHORT_END_INDEX));
    }

    @SuppressWarnings("unused")
    public String getName() {
        return name;
    }

    @SuppressWarnings("unused")
    public Temperature getTemp() {
        return temp;
    }

    @SuppressWarnings("unused")
    public int getShelfLife() {
        return shelfLife;
    }

    @SuppressWarnings("unused")
    public float getDecayRate() {
        return decayRate;
    }

    /**
     * @return shelf string
     */
    public String getShelf() {
        return shelf;
    }

    /**
     * @return Shelf enum
     */
    public Shelf getShelfEnum() {
        return Shelf.valueOf(shelf);
    }

    public LocalDateTime getTime() {
        return time;
    }

    // For hashmap, we want the equals and hashcode.
    @Override
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof Order)) {
            return false;
        }
        Order other = (Order) o;
        return this.id.equals(other.id);
    }

    @Override
    public final int hashCode() {
        // to simplify, sum the first few chars
        int sum = 0;
        for (int i = 0; i < id.length() && i < 7; i++) {
            sum += id.charAt(i) - 'a';
        }
        return sum;
    }

    @Override
    public String toString() {
        return "Order{" +
            "id='" + getId() + '\'' +
            ", temp=" + String.format("%-6s", temp) +
            ", v=" + String.format("%7.2f", getValue()) +
            ", name='" + String.format("%-12s", name) + '\'' +
            '}';
    }

    @SuppressWarnings("unused")
    public String toStringFull() {
        return "Order{" +
            "id='" + getId() + '\'' +
            ", name='" + name + '\'' +
            ", temp=" + temp +
            ", v=" + String.format("%#.2f", getValue()) +
            ", shelfLife=" + shelfLife +
            ", decayRate=" + decayRate +
            '}';
    }
}

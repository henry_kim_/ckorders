package com.ckit.event;

import com.ckit.model.Order;

/**
 * The {@link OrderReceivedEvent} should should be dispatched whenever an order is received.
 */
public class OrderReceivedEvent extends AbstractEvent {

    private final Order order;

    public OrderReceivedEvent(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }
}

package com.ckit;

import org.junit.jupiter.api.Test;

/**
 * Tests that the app starts without errors from Spring.
 * This doesn't test functionality.
 */
public class AppTest {

    @Test
    public void test() {
        Application.main(new String[]{});
    }
}

package com.ckit.framework;

import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.verify;

import com.ckit.event.OrderPickedUpEvent;
import com.ckit.event.OrderReceivedEvent;
import com.ckit.handler.OrderPickedUpEventHandler;
import com.ckit.handler.OrderReceivedEventHandler;
import com.ckit.model.Order;
import com.ckit.ShelfManager;
import org.junit.jupiter.api.Test;

public class EventDispatcherOrderReceivedTest {

    /**
     * register events and event handlers with event dispatcher.
     * events should be dispatched.
     */
    @Test
    public void testEventDrivenPattern() {

        var dispatcher = spy(EventDispatcher.getInstance());
        ShelfManager.init(dispatcher);
        var shelfManager = spy(ShelfManager.getInstance());
        var orderReceivedEventHandler = spy(new OrderReceivedEventHandler(shelfManager));
        var orderPickedUpEventHandler = spy(new OrderPickedUpEventHandler());
        dispatcher.registerHandler(OrderReceivedEvent.class, orderReceivedEventHandler);
        dispatcher.registerHandler(OrderPickedUpEvent.class, orderPickedUpEventHandler);

        var order = new Order("22", "cheese pizza", "hot", 300, 0.45f);

        var orderReceivedEvent = new OrderReceivedEvent(order);
        var orderPickedUpEvent = new OrderPickedUpEvent(order);

        //fire a orderReceivedEvent and verify that orderReceivedEventHandler has been invoked.
        dispatcher.dispatch(orderReceivedEvent);
        verify(dispatcher).dispatch(orderReceivedEvent);
        verify(orderReceivedEventHandler).onEvent(orderReceivedEvent);

        //fire a orderPickedUpEvent and verify that orderPickedUpEventHandler has been invoked.
        dispatcher.dispatch(orderPickedUpEvent);
        verify(dispatcher).dispatch(orderPickedUpEvent);
        verify(orderPickedUpEventHandler).onEvent(orderPickedUpEvent);
    }

}

# README #

To run the app, here are the steps.

### How to use maven with this app

* Build application
```
mvn clean compile
```

* Run application with Maven command line
```
mvn exec:java
```

* Unit tests
```
mvn test
```

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact

### Autowired or not ###

UNIT TEST notes:

This article says to provide a constructor allows me to pass in an instance of dependencies.
This means let's not use the '@A-u-t-o-wired' annotation.

 https://reflectoring.io/unit-testing-spring-boot/

 https://blog.kwnetapps.com/constructor-injection-nomore-autowired/

 It can be painful to test when you’re using a lot of a-u-t-o-wired fields. You need to create
 mocks with stubs and make sure they’re injected with your mock DI container. This ends up
 requiring more overhead and more headaches trying to get testing to work.

 After switching a few projects from a-u-t-o-wired to constructor injection I got a huge uptick in
 unit tests and coverage. More time coding, less time fighting with Junit/Spring, tests became
 way easier to write.

### Articles ###

without a-u-t-o-w-i-r-e-d:
https://github.com/thombergs/code-examples/blob/master/spring-boot/dependency-injection/src/main/java/com/example/ExampleApplication.java


https://reflectoring.io/spring-boot-application-events-explained/

BlockingQueue<String> blockingQueue = new LinkedBlockingDeque<>();
Can change this to bounded queue by passing a capacity argument to constructor.

http://giocc.com/writing-an-event-driven-framework-with-java.html
   https://stackoverflow.com/questions/13483048/creating-a-simple-event-driven-architecture

https://www.javacodegeeks.com/2016/03/simple-event-driven-design.html

github of design patterns:
https://github.com/iluwatar/java-design-patterns/tree/master/event-driven-architecture/src/main/java/com/iluwatar/eda/framework
https://www.baeldung.com/java-blocking-queue

http://web.mit.edu/6.031/www/sp17/classes/22-queues/

https://www.confluent.io/blog/journey-to-event-driven-part-2-programming-models-event-driven-architecture/
  lots of good diagrams.

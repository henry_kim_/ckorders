package com.ckit.handler;

import com.ckit.event.CourierArrivedEvent;
import com.ckit.event.DispatchCourierEvent;
import com.ckit.framework.EventDispatcher;
import com.ckit.framework.Handler;
import java.util.Random;
import java.util.concurrent.Callable;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class DispatchCourierEventHandler implements Handler<DispatchCourierEvent> {

    private static final Logger LOG = LogManager.getRootLogger();
    private final Random random = new Random();
    private final int randomRangeStart;
    private final int randomRangeEnd;
    private final EventDispatcher dispatcher;

    public DispatchCourierEventHandler(EventDispatcher dispatcher,
        int randomRangeStart, int randomRangeEnd) {
        this.dispatcher = dispatcher;
        this.randomRangeStart = randomRangeStart;
        this.randomRangeEnd = randomRangeEnd;
        LOG.info("ctor {} randomRangeStart {} End {} ", getClass().getSimpleName(),randomRangeStart, randomRangeEnd);
    }

    /**
        Dispatch a courier, incorporating a random arrival delay.

        note: For faster test processing, can use short 500 milliseconds.
        note: resultFuture.get() will block others.
     */
    @Override
    public void onEvent(DispatchCourierEvent event) {
        String prefix = "DispatchCourier";

        Callable<String> callableTask = () -> {
            dispatcher.dispatch(new CourierArrivedEvent(event.getOrder().getId()));
            return "execution";
        };

        int delaySeconds = getRandomNumberInRange(randomRangeStart, randomRangeEnd);
        LOG.info("{} dispatch courier. {} secs. id '{}'", prefix, delaySeconds, event.getOrder().getId());

        final ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.schedule(callableTask, delaySeconds * 1000, TimeUnit.MILLISECONDS);
        executor.shutdown();
    }

    private int getRandomNumberInRange(int min, int max) {
        return min + random.nextInt((max - min) + 1);
    }

}

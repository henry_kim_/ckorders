package com.ckit.model;

import com.ckit.ApplicationUtil;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SuppressWarnings("unused")
public enum Shelf {
    Hot(Temperature.hot, 10),
    Cold(Temperature.cold, 10),
    Frozen(Temperature.frozen, 10),
    Overflow(Temperature.any, 15);

    private static final Logger LOG = LogManager.getLogger();

    private final int capacity;
    private final Map<String, Order> orders;

    Shelf(Temperature t, int capacity) {
        // Using LinkedHashMap in case there's a need to see the insertion order.
        this.orders = new LinkedHashMap<>();
        String propName = "capacity-" + t.name();
        int number = Integer.parseInt(ApplicationUtil.getProps().getProperty(propName, "-1"));
        if (number < 0) {
            System.err.println("missing config "+ propName);
            System.exit(1);
        }
        this.capacity = number;
    }

    public boolean isFull() {
        return orders.size() >= capacity;
    }

    public int size() {
        return orders.size();
    }

    public int getCapacity() {
        return capacity;
    }

    public void clear() {
        orders.clear();
    }
    /**
     * Add order to current shelf.
     */
    public boolean add(Order order) {
        if (isFull()) {
            return false;
        }
        orders.put(order.getId(), order);
        return true;
    }

    public boolean remove(String id) {
        return null != orders.remove(id);
    }

    /**
     * @return entry set of orders map
     */
    public Set<Map.Entry<String, Order>>  getOrders() {
        return orders.entrySet();
    }

    /**
     * @return key set of orders map
     */
    public Set<String> getOrderKeySet() {
        return orders.keySet();
    }

    /**
     * @param id order id
     * @return return the Order with given id, from this shelf
     */
    public Order getOrderById(String id) {
        return orders.get(id);
    }

    /**
     * @param id order id
     * @return return true if Order id exists in this shelf
     */
    public boolean containsId(String id) {
        return orders.containsKey(id);
    }

    public static Shelf getShelfByOrderTemp(Order order) {
        return Shelf.getShelfByTemp(order.getTemp());
    }

    static Shelf getShelfByTemp(Temperature t) {
        if (t == Temperature.hot) {
            return Hot;
        }
        if (t == Temperature.cold) {
            return Cold;
        }
        if (t == Temperature.frozen) {
            return Frozen;
        }
        return Overflow;
    }

}
// abstract int execute(int num1, int num2);

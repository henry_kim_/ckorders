package com.ckit.model;

import static org.assertj.core.api.Assertions.assertThat;

import com.ckit.ApplicationUtil;
import java.time.LocalDateTime;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class ShelfTest {

    @BeforeAll
    static void beforeAll() {
        ApplicationUtil.getProps();
    }

    @BeforeEach
    void beforeEach() {
        Shelf shelf = Shelf.Cold;
        shelf.clear();
    }

    @Test
    void addRemove() {
        Shelf shelf = Shelf.Cold;
        assertThat(shelf.isFull()).isFalse();
        Order order = getUnExpiredColdOrder();

        String id = order.getId();
        shelf.add(order);

        assertThat(shelf.size()).isEqualTo(1);
        Order actual = shelf.getOrderById(id);
        assertThat(actual).isEqualTo(order);

        // test that order.shelf was set by the 'add'.
        assertThat(actual.getShelf()).isEqualTo("Cold");

        // remove
        shelf.remove(id);
        assertThat(shelf.size()).isEqualTo(0);
    }

    @Test
    void addFailsWhenFull() {
        Shelf shelf = Shelf.Cold;
        for (int i = 0; i < shelf.getCapacity(); i++) {
            shelf.add(new Order("a" + i, "orange", "cold", 200, 0.3f, LocalDateTime.now(),
                Shelf.Cold.name()));
        }
        assertThat(shelf.isFull()).isTrue();

        Order order = new Order("a99", "orange", "cold", 200, 0.3f, LocalDateTime.now(),
            Shelf.Cold.name());
        assertThat(shelf.add(order)).isFalse();
    }

    @Test
    void getShelfByTemp() {
        assertThat(Shelf.getShelfByTemp(Temperature.hot)).isEqualTo(Shelf.Hot);
        assertThat(Shelf.getShelfByTemp(Temperature.cold)).isEqualTo(Shelf.Cold);
        assertThat(Shelf.getShelfByTemp(Temperature.frozen)).isEqualTo(Shelf.Frozen);
        assertThat(Shelf.getShelfByTemp(Temperature.any)).isEqualTo(Shelf.Overflow);

        Order order = getUnExpiredColdOrder();
        assertThat(Shelf.getShelfByOrderTemp(order)).isEqualTo(Shelf.Cold);
    }

    private Order getUnExpiredColdOrder() {
        Order order = new Order("good1", "orange", "cold", 100, 1.0f, LocalDateTime.now().plusSeconds(-1),
            Shelf.Cold.name());
        assertThat(order.isWasted()).isFalse();
        return order;
    }
}

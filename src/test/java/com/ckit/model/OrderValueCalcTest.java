package com.ckit.model;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDateTime;
import org.junit.jupiter.api.Test;

class OrderValueCalcTest {

    @Test
    void orderValue() {
        Order order = new Order("a0", "orange", "cold", 100, 0.5f, LocalDateTime.now(), Shelf.Cold.name());
        LocalDateTime time = order.getTime().plusSeconds(4);
        assertThat(OrderValueCalc.orderValue(order, time)).isEqualTo(0.98d);
    }

    @Test
    void calcOrderValue() {
        Order order = new Order("a0", "orange", "cold", 100, 0.5f, LocalDateTime.now(), Shelf.Cold.name());
        double shelfDecayModifier = 1.0d;
        // (100 - (0.5 * 4 * 1.0)) / 100 = 1.0
        assertThat(OrderValueCalc.calcOrderValue(order, 4, shelfDecayModifier)).isEqualTo(0.98d);

        shelfDecayModifier = 2.0d;
        // (100 - (0.5 * 4 * 2.0)) / 100 = 1.0
        assertThat(OrderValueCalc.calcOrderValue(order, 4, shelfDecayModifier)).isEqualTo(0.96d);
    }

    @Test
    void getAge() {
        LocalDateTime time1 = LocalDateTime.now();
        LocalDateTime time2 = time1.plusSeconds(1);
        assertThat(OrderValueCalc.getAge(time1, time2)).isEqualTo(1);
    }

    @Test
    void getShelfDecayModifier() {
        Order order = new Order("good1", "orange", "cold", 100, 1.0f, LocalDateTime.now(),
            Shelf.Cold.name());
        assertThat(OrderValueCalc.getShelfDecayModifier(order)).isEqualTo(1);

        order = Order.copyWithShelf(order, Shelf.Overflow.name());
        assertThat(OrderValueCalc.getShelfDecayModifier(order)).isEqualTo(2);
    }
}

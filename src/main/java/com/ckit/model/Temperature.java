package com.ckit.model;

public enum Temperature {
    hot,
    cold,
    frozen,
    any
}

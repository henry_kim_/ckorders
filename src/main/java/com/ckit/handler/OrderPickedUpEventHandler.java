package com.ckit.handler;

import com.ckit.event.OrderPickedUpEvent;
import com.ckit.framework.Handler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OrderPickedUpEventHandler implements Handler<OrderPickedUpEvent> {
    private static final Logger LOG = LogManager.getRootLogger();

    public OrderPickedUpEventHandler(){
        LOG.info("ctor {}", getClass().getSimpleName());
    }

    @Override
    public void onEvent(OrderPickedUpEvent event) {
        String prefix = "OrderPickedUpEvent.";
        LOG.info("{} no-op", prefix);
    }
}

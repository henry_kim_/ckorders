package com.ckit;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

@SuppressWarnings("SameReturnValue")
public class ApplicationUtil {

    public static final String CONFIG_FILE_NAME = "config.properties";
    public static final Properties appProps = new Properties();

    public static Properties getProps() {
        return getProps(CONFIG_FILE_NAME);
    }
    /**
     * @param configFileName properties file under resources folder.
     * @return Properties found in the given file
     */
    public static Properties getProps(@SuppressWarnings("SameParameterValue") String configFileName) {
        if (appProps.size() > 0) {
            return appProps;
        }
        InputStream is = Thread.currentThread().getContextClassLoader().getResourceAsStream(configFileName);
        try {
            appProps.load(is);
            for (String p : appProps.stringPropertyNames()) {
                System.out.println("resource " + configFileName+" property " + p + " = " + appProps.getProperty(p));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return appProps;
    }
}

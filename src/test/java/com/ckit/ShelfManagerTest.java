package com.ckit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import com.ckit.event.DispatchCourierEvent;
import com.ckit.event.OrderDiscardedEvent;
import com.ckit.event.OrderPickedUpEvent;
import com.ckit.framework.EventDispatcher;
import com.ckit.model.Order;
import com.ckit.model.Shelf;
import java.time.LocalDateTime;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class ShelfManagerTest {
    @Mock
    private EventDispatcher dispatcher;

    @InjectMocks
    private ShelfManager mgr;

    @BeforeEach
    public void beforeEach() {
        System.out.println("BeforeEach");
        MockitoAnnotations.initMocks(this);
        ShelfManager.init(dispatcher);
        mgr = ShelfManager.getInstance();
    }

    @AfterEach
    public void afterEach() {
        System.out.println("AfterEach");
    }

    @Test
    void pickupOrder() {
        doNothing().when(dispatcher).dispatch(any());

        Order order = getUnExpiredColdOrder();
        String id = order.getId();

        assertThat(Shelf.Cold.size()).isEqualTo(0);

        mgr.handleOrderReceived(order);
        verify(dispatcher, times(1)).dispatch(any(DispatchCourierEvent.class));

        assertThat(Shelf.Cold.size()).isEqualTo(1);

        mgr.pickupOrder(id, "log");
        verify(dispatcher, times(1)).dispatch(any(OrderPickedUpEvent.class));

        assertThat(mgr.getOrderById(id)).isNull();
        assertThat(Shelf.Cold.size()).isEqualTo(0);
    }

    @Test
    void pickupOrderThatBecameWasteAtPickupTriggersDiscardedEvent() {
        // Create an expired order.
        // When courier is picking up, we see that the order should be discarded as waste.
        // Assert that OrderDiscardedEvent is triggered.

        doNothing().when(dispatcher).dispatch(any());

        Order order = getExpiredOrder();
        String id = order.getId();

        mgr.handleOrderReceived(order);
        verify(dispatcher, times(1)).dispatch(any(DispatchCourierEvent.class));

        mgr.pickupOrder(id, "log");
        verify(dispatcher, times(1)).dispatch(any(OrderDiscardedEvent.class));

        assertThat(mgr.getOrderById(id)).isNull();
    }

    @Test
    void handleOrderReceived() {
        doNothing().when(dispatcher).dispatch(any());

        Order order = getUnExpiredColdOrder();
        Shelf shelf = Shelf.Cold;
        shelf.add(order);

        mgr.handleOrderReceived(order);
        verify(dispatcher, times(1)).dispatch(any(DispatchCourierEvent.class));

        assertThat(mgr.getOrderById(order.getId()).getId()).isEqualTo(order.getId());
        // Verify this specific shelf.
        assertThat(Shelf.Cold.containsId(order.getId())).isTrue();
    }

    @Test
    void placeBestShelf() {
        Order order = getUnExpiredColdOrder();
        boolean result = mgr.placeBestShelf(order);
        assertThat(result).isTrue();
        assertThat(order.getShelf()).isEqualTo(Shelf.Cold.name());
        assertThat(mgr.size()).isEqualTo(1);

        assertThat(mgr.getOrderById(order.getId()).getId()).isEqualTo(order.getId());
        // Verify this specific shelf.
        assertThat(Shelf.Cold.containsId(order.getId())).isTrue();

    }

    @Test
    void attemptAddShelf_isFull() {
        String prefix = "log";
        Shelf shelf = Shelf.Cold;
        testCannotAddShelf(prefix, shelf);
    }

    @Test
    void addOverflow() {
        String prefix = "log";
        Order order = getUnExpiredColdOrder();

        boolean result = mgr.crudAddOverflow(order, prefix);

        assertThat(result).isTrue();
        assertThat(mgr.getOrderById(order.getId()).getId()).isEqualTo(order.getId());
        // Verify this specific shelf.
        assertThat(Shelf.Overflow.containsId(order.getId())).isTrue();
    }

    @Test
    void addOverflow_isFull() {
        String prefix = "log";
        Shelf shelf = Shelf.Overflow;
        testCannotAddShelf(prefix, shelf);
    }

    private void testCannotAddShelf(String prefix, Shelf shelf) {
        for (int i = 0; i < shelf.getCapacity(); i++) {
            shelf.add(new Order("a" + i, "orange", "cold", 200, 0.3f, LocalDateTime.now(),
                shelf.name()));
        }
        assertThat(shelf.isFull()).isTrue();

        Order order = getUnExpiredColdOrder();

        boolean result = mgr.attemptAddShelf(shelf, order, prefix);

        assertThat(result).isFalse();
        assertThat(mgr.getOrderById(order.getId())).isEqualTo(null);
    }

    @Test
    void crudDeleteOrder() {
        String prefix = "log";
        Order order = getUnExpiredColdOrder();
        order = Order.copyWithShelf(order, Shelf.Overflow.name());
        boolean result = mgr.crudAddOverflow(order, prefix);
        assertThat(result).isTrue();

        mgr.crudDeleteOrder(order);

        assertThat(mgr.getOrderById(order.getId())).isEqualTo(null);
        assertThat(Shelf.Overflow.containsId(order.getId())).isFalse();
    }

    @Test
    void discardRandomOverflow() {
        String prefix = "log";
        Shelf shelf = Shelf.Overflow;
        assertThat(shelf.size()).isEqualTo(0);
        testCannotAddShelf(prefix, shelf);
        int capacity = shelf.getCapacity();
        assertThat(shelf.size()).isEqualTo(capacity);

        // call the method enough times to make the overflow shelf empty.
        for (int expectedSize = capacity; expectedSize > 0; expectedSize--) {

            assertThat(mgr.discardRandomOverflow(prefix)).isEqualTo(1);
            assertThat(shelf.size()).isEqualTo(expectedSize - 1);
        }
        assertThat(shelf.size()).isEqualTo(0);

        // test that when list is empty, we safeguard to stop getting a random index.
        int resultCount = mgr.discardRandomOverflow(prefix);
        assertThat(resultCount).isEqualTo(0);
    }

    @Test
    void attemptShiftOverflowToHome() {
        String prefix = "log";
        Order newOrder = getUnExpiredColdOrder();
        Shelf shelf = Shelf.Overflow;
        testCannotAddShelf(prefix, shelf);
        int capacity = Shelf.Overflow.getCapacity();

        // Cold size is 0.
        assertThat(Shelf.Cold.size()).isEqualTo(0);
        assertThat(Shelf.Overflow.size()).isEqualTo(capacity);

        boolean result = mgr.attemptMoveOverflowToHomeAddOverflow(newOrder, prefix);

        // Cold size is 1.   We moved item from overflow into cold.  Overflow size is same.
        assertThat(result).isTrue();
        assertThat(Shelf.Cold.size()).isEqualTo(1);
        assertThat(Shelf.Overflow.size()).isEqualTo(capacity);
    }

    @Test
    void discardShelvesExceptOwnShelf() {
        int discardCount;
        doNothing().when(dispatcher).dispatch(any());

        Order order = getExpiredOrder();
        Shelf shelf = Shelf.getShelfByOrderTemp(order);
        shelf.add(order);

        // nothing discarded since expired is on same shelf as parameter.
        discardCount = mgr.discardShelvesExcept(shelf, "some");
        assertThat(discardCount).isEqualTo(0);

        // discarded since on different shelf as the parameter.
        discardCount = mgr.discardShelvesExcept(Shelf.Hot, "some");
        assertThat(discardCount).isEqualTo(1);
    }

    @Test
    void discardShelvesExceptOverflow() {
        int discardCount;
        doNothing().when(dispatcher).dispatch(any());

        // Put an expired order on overflow. then after the call, make sure the overflow item
        // still exists.
        Order order = new Order("expiredOverflow1", "orange", "cold", 100, 1.0f, LocalDateTime.now().plusSeconds(-100),
            Shelf.Overflow.name());
        Shelf shelf = Shelf.Overflow;
        shelf.add(order);

        // nothing discarded since expired is on overflow.
        discardCount = mgr.discardShelvesExcept(Shelf.Hot, "some");
        assertThat(discardCount).isEqualTo(0);
    }

    @Test
    void discardInShelf() {
        doNothing().when(dispatcher).dispatch(any());

        Order order = getExpiredOrder();
        Shelf shelf = Shelf.Cold;
        shelf.add(order);
        mgr.discardInShelf(shelf, "some");

        verify(dispatcher, times(1)).dispatch(any(OrderDiscardedEvent.class));
    }

    private Order getUnExpiredColdOrder() {
        return new Order("good1", "orange", "cold", 100, 1.0f, LocalDateTime.now(),
            Shelf.Cold.name());
    }

    private Order getExpiredOrder() {
        return new Order("expired1", "orange", "cold", 100, 1.0f, LocalDateTime.now().plusSeconds(-100),
            Shelf.Cold.name());
    }
}

package com.ckit.model;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import org.apache.commons.lang3.StringUtils;

/**
 * Class design notes:
     1. Break out to smaller methods for simple testing, with less mocks.

     2. Did not put orderValue in the Order class so that we can accommodate business rule changes
     outside of the model class.
 */
public class OrderValueCalc {
    static final int OVERFLOW_DECAY_MODIFIER = 2;

    /**
     * @param order Order
     * @param now current time
     * @return order value based on time passed in.
     */
    public static double orderValue(Order order, LocalDateTime now) {
        long orderAge = getAge(order.getTime(), now);
        double shelfDecayModifier = getShelfDecayModifier(order);
        return calcOrderValue(order, orderAge, shelfDecayModifier);
    }

    static double calcOrderValue(Order order, long orderAge, double shelfDecayModifier) {
        double v = order.getShelfLife() - order.getDecayRate() * orderAge * shelfDecayModifier;
        return v / order.getShelfLife();
    }

    static long getAge(LocalDateTime earlier, LocalDateTime now) {
        return ChronoUnit.SECONDS.between(earlier, now);
    }

    // 2 : overflow shelf.  1 : any other
    // To adjust test behavior, can adjust these values.
    static double getShelfDecayModifier(Order order) {
        return StringUtils.equals(Shelf.Overflow.name(), order.getShelf())
            ? OVERFLOW_DECAY_MODIFIER : 1;
    }

}

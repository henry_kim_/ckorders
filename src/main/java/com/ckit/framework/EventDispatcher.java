package com.ckit.framework;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Handles the routing of {@link Event} messages to associated handlers. A {@link HashMap} is used
 * to store the association between events and their respective handlers.
 *
 * Spring framework sample:
 * http://zetcode.com/spring/bean/
 */
@Component
@Scope(value = ConfigurableBeanFactory.SCOPE_SINGLETON)
public class EventDispatcher {
    private static final Logger LOG = LogManager.getRootLogger();

    private final Map<Class<? extends Event>, Handler<? extends Event>> handlers;
    private static EventDispatcher singleton;

    private EventDispatcher() {
        handlers = new HashMap<>();
        LOG.info("ctor {}", this.getClass().getSimpleName());
    }

    public static EventDispatcher getInstance() {
        if (singleton == null) {
            synchronized (LOG) {
                singleton = new EventDispatcher();
            }
        }
        return singleton;
    }

    /**
     * Links an {@link Event} to a specific {@link Handler}.
     *
     * @param eventType The {@link Event} to be registered
     * @param handler The {@link Handler} that will be handling the {@link Event}
     */
    public <E extends Event> void registerHandler(
        Class<E> eventType,
        Handler<E> handler
    ) {
        handlers.put(eventType, handler);
    }

    /**
     * Dispatches an {@link Event} depending on it's type.
     *
     * @param event The {@link Event} to be dispatched
     */
    @SuppressWarnings("unchecked")
    public <E extends Event> void dispatch(E event) {
        var handler = (Handler<E>) handlers.get(event.getClass());
        if (handler != null) {
            handler.onEvent(event);
        } else {
            LOG.error("unknown event: {}", event.getClass().getSimpleName());
        }
    }

}

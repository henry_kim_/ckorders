package com.ckit.event;

import com.ckit.framework.Event;
import com.ckit.framework.EventDispatcher;

/**
 * The {@link AbstractEvent} class serves as a base class for defining custom events.
 * Events are distinguished with {@link #getType() getType}.
 */
public abstract class AbstractEvent implements Event {

    public static String getEventPrefix(Event event) {
        return event.getClass().getSimpleName().replaceAll("Event", "");
    }

    /**
     * this method is used by the {@link EventDispatcher} to dispatch events depending on their type.
     */
    @SuppressWarnings("unused")
    public Class<? extends Event> getType() {
        return getClass();
    }
}

package com.ckit.event;

import com.ckit.model.Order;

/**
 * The {@link DispatchCourierEvent} should should be fired when an order is received.
 * This class can hold the logic to specify the random number of seconds (2 to 6)
 * it will take for the courier arrival.
 *
 * On arrival event, courier will pickup, and instantly deliver it.
 */
public class DispatchCourierEvent extends AbstractEvent {

    private final Order order;

    public DispatchCourierEvent(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }
}

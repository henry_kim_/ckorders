package com.ckit.handler;

import com.ckit.event.CourierArrivedEvent;
import com.ckit.framework.Handler;
import com.ckit.ShelfManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class CourierArrivedEventHandler implements Handler<CourierArrivedEvent> {
    private static final Logger LOG = LogManager.getRootLogger();

    private final ShelfManager shelfManager;

    public CourierArrivedEventHandler(ShelfManager shelfManager) {
        this.shelfManager = shelfManager;
        LOG.info("ctor {}", getClass().getSimpleName());
    }

    @Override
    public void onEvent(CourierArrivedEvent event) {
        String prefix = "CourierArrived.";
        shelfManager.pickupOrder(event.getId(), prefix);
    }

}

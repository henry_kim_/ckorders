package com.ckit.event;

import com.ckit.model.Order;

/**
 * The {@link OrderDiscardedEvent} should should be dispatched whenever an order is discarded.
 */
@SuppressWarnings("unused")
public class OrderDiscardedEvent extends AbstractEvent {

    private final Order order;

    public OrderDiscardedEvent(Order order) {
        this.order = order;
    }

    public Order getOrder() {
        return order;
    }
}

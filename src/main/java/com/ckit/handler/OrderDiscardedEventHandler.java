package com.ckit.handler;

import com.ckit.event.AbstractEvent;
import com.ckit.event.OrderDiscardedEvent;
import com.ckit.framework.Handler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class OrderDiscardedEventHandler implements Handler<OrderDiscardedEvent> {
    private static final Logger LOG = LogManager.getRootLogger();

    public OrderDiscardedEventHandler() {
        LOG.info("ctor {}", getClass().getSimpleName());
    }

    @Override
    public void onEvent(OrderDiscardedEvent event) {
        String prefix = AbstractEvent.getEventPrefix(event);
        LOG.info("{}. refund and cancel courier for '{}'", prefix, event.getOrder().getId());
    }

}

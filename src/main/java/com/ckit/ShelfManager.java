package com.ckit;

import static com.ckit.model.Shelf.Cold;
import static com.ckit.model.Shelf.Frozen;
import static com.ckit.model.Shelf.Hot;
import static com.ckit.model.Shelf.Overflow;

import com.ckit.event.DispatchCourierEvent;
import com.ckit.event.OrderDiscardedEvent;
import com.ckit.event.OrderPickedUpEvent;
import com.ckit.framework.EventDispatcher;
import com.ckit.model.Order;
import com.ckit.model.Shelf;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Random;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
    Singleton class to manage the all shelves. All updates to the shelves occur through this class.
    A-u-t-o-wire notes:  See Readme
 */
public class ShelfManager {
    private static final Logger LOG = LogManager.getRootLogger();

    private final LinkedHashMap<String, Order> orderIdMap = new LinkedHashMap<>();
    private final List<Shelf> shelves = Arrays.asList(Hot, Cold, Frozen, Overflow);
    private final Comparator<Order> valueComparator = Comparator.comparingDouble(Order::getValue);
    private final Random random = new Random();
    private final EventDispatcher dispatcher;

    private static ShelfManager singleton;

    private ShelfManager(EventDispatcher dispatcher) {
        this.dispatcher = dispatcher;
        LOG.info("ctor {}", getClass().getSimpleName());
    }

    public synchronized static ShelfManager init(EventDispatcher dispatcher) {
        // Comment this out so tests can setup mock variations.
        //if (singleton != null) {
        //    throw new AssertionError("already initialized");
        //}
        singleton = new ShelfManager(dispatcher);
        singleton.clearShelves();
        return singleton;
    }

    public static ShelfManager getInstance() {
        if (singleton == null) {
            throw new AssertionError("call init first");
        }
        return singleton;
    }

    public Order getOrderById(String id) {
        return orderIdMap.get(id);
    }

    public int size() {
        return orderIdMap.size();
    }

    @SuppressWarnings("unused")
    public List<Shelf> getShelves() {
        return Collections.unmodifiableList(shelves);
    }

    public void clearShelves() {
        for (Shelf shelf : shelves) {
            shelf.clear();
        }
        orderIdMap.clear();
    }

    /**
     * Pick up the order.
     * Triggered by DispatchCourierEvent.
     * @param id Order id
     * @param prefix log prefix
     */
    public void pickupOrder(String id, String prefix) {
        Order order = orderIdMap.get(id);
        if (order == null) {
            LOG.info("{} not exist, perhaps discarded {}", prefix, id);
            return;
        }
        if (crudDeleteOrder(order)) {
            showShelves();
            Shelf shelf = order.getShelfEnum();
            if (order.isWasted()) {
                LOG.info("{} discarded waste '{}' sz {} {}", prefix, shelf, shelf.size(), order);
                dispatcher.dispatch(new OrderDiscardedEvent(order));
            } else {
                LOG.info("{} fetched '{}' sz {} {}", prefix, shelf, shelf.size(), order);
                dispatcher.dispatch(new OrderPickedUpEvent(order));
                // Delivery event: Don't log since it's instant, same as picked-up.
            }
        }
    }

    /**
     * Triggered by OrderReceivedEvent.

     * Place the order on best-available shelf.
     * Treat all steps as one transaction.
     * Dispatch a courier.
     * @param order Order
     */
    public void handleOrderReceived(Order order) {
        if (placeBestShelf(order)) {
            dispatcher.dispatch(new DispatchCourierEvent(order));
        }
    }

    /**
     * Place order on best-available shelf,based on several rules.
     * This is a separate method that doesn't involve dispatching an vent.
     * @param order Order
     * @return boolean true if order was cooked and placed on shelf. false on any exception.
     */
    boolean placeBestShelf(Order order) {
        synchronized (orderIdMap) {
            String prefix = "OrderReceived.";

            // 1. Home shelf:   (best-available shelf)
            //    Discard all waste, and attempt to add.

            Shelf shelf = Shelf.getShelfByOrderTemp(order);
            if (attemptAddShelf(shelf, order, prefix)) {
                return true;
            }

            // 2. overflow shelf:
            //    Discard all waste, and attempt to add.

            prefix = prefix + "FULL";
            if (attemptAddShelf(Overflow, order, prefix)) {
                return true;
            }

            // 3. Shelves other than preferred and overflow:  discard all waste.

            int discardCount = discardShelvesExcept(shelf, prefix);

            // 4. Shift from overflow shelf to its home shelf. Start with lowest order value.
            //    If the move was possible, then add new order to overflow.

            if (attemptMoveOverflowToHomeAddOverflow(order, prefix)) {
                return true;
            }

            // 5. No shelves have room for one overflow order, so discard a random overflow order.

            discardCount += discardRandomOverflow(prefix);

            // 6. Add new order to overflow.  We expect this to succeed.

            if (crudAddOverflow(order, prefix)) {
                return true;
            }

            LOG.error("{} Catch-all message. DiscardCount: {} Failed to add order {}",
                prefix, discardCount, order);
            return false;
        }
    }

    /**
     * Log the contents of all shelves.
     */
    void showShelves() {
        synchronized (orderIdMap) {
            for (Shelf shelf : shelves) {
                String shelfName = String.format("%-8s", shelf);
                LOG.info("  shelf {} sz {} {}", shelfName, shelf.size(), shelf.isFull() ? "full":"");
                for (Entry<String, Order> entry : shelf.getOrders()) {
                    LOG.info("        " + shelfName + " " + entry.getValue());
                }
            }
            LOG.info("");
        }
    }

    boolean attemptAddShelf(Shelf shelf, Order orderOrig, String prefix) {
        // Before trying to add to shelf, discard any waste in this shelf.
        discardInShelf(shelf, prefix);
        if (!shelf.isFull()) {
            Order order = Order.copyWithShelf(orderOrig, shelf.name());
            shelf.add(order);
            orderIdMap.put(order.getId(), order);
            LOG.info("{} added to shelf '{}' {}", prefix, shelf, order);
            showShelves();
            return true;
        }
        return false;
    }

    boolean crudAddOverflow(Order orderOrig, String prefix) {
        if (!Overflow.isFull()) {
            Order order = Order.copyWithShelf(orderOrig, Overflow.name());
            Overflow.add(order);
            orderIdMap.put(order.getId(), order);
            LOG.info("{} add to overflow {}", prefix, order);
            showShelves();
            return true;
        }
        return false;
    }

    /**
     * Delete order from all structures. (map, shelf list)
     * @param order Order
     */
    boolean crudDeleteOrder(Order order) {
        synchronized (orderIdMap) {
            // 1. remove from map
            orderIdMap.remove(order.getId());
            // 2. remove from shelf list.
            Shelf shelf = order.getShelfEnum();
            return shelf.remove(order.getId());
        }
    }

    int discardRandomOverflow(String prefix) {
        Shelf shelf = Shelf.Overflow;
        if (shelf.size() < 1) {
            return 0;
        }
        List<String> keyList = new ArrayList<>(shelf.getOrderKeySet());
        int randomIndex = random.nextInt(keyList.size());
        String id = keyList.get(randomIndex);
        Order discardedOrder = shelf.getOrderById(id);
        if (crudDeleteOrder(discardedOrder)) {
            LOG.info("{} discarded random order from '{}' sz {} {}", prefix, shelf, shelf.size(), discardedOrder);
            dispatcher.dispatch(new OrderDiscardedEvent(discardedOrder));
            showShelves();
            return 1;
        }
        return 0;
    }

    /**
     * Return true if we're able to shift an overflow item to home shelf,
     * and move newOrder to overflow.
     *
     * "Home shelf" is the shelf that matches an newOrder's temperature.

     "ordering"

     Examine orders starting with lowest shelf life. Pick the order that is closest to
     being discarded.
     if allowable shelf has room, then move it.

     "priority queue?"

     Since the newOrder value uses current time to compute, the priority queue
     i doubt can be used since the ordering in the heap will be stale, when
     we query after heap insert.  Therefore, sort the shelf orders at the time we need
     the ordering.
     */
    boolean attemptMoveOverflowToHomeAddOverflow(Order orderOrig, String prefix) {

        List<Order> sortedOverflowByValue = getOrderListFromShelf(Overflow);
        // SORT by lowest shelf life, so we discard in the order with least value.

        sortedOverflowByValue.sort(valueComparator);

        for (Order overflowOrder : sortedOverflowByValue) {
            // Can we move the overflow item to the original shelf?
            Shelf temperatureShelf = Shelf.getShelfByOrderTemp(overflowOrder);
            if (!temperatureShelf.isFull()) {
                Order newOrder = Order.copyWithShelf(orderOrig, Overflow.name());
                temperatureShelf.add(overflowOrder);
                // Above, we added overflow item to home. Now remove from overflow.
                Overflow.remove(overflowOrder.getId());
                LOG.info("{} moved overflow to '{}' {}", prefix, temperatureShelf, overflowOrder);

                // We moved out 1 from overflow, so add new newOrder to overflow.

                Overflow.add(newOrder);
                orderIdMap.put(newOrder.getId(), newOrder);
                LOG.info("{} after moving out of overflow, added to shelf '{}' {}", prefix, Overflow, newOrder);
                showShelves();
                return true;
            }
        }
        return false;
    }

    List<Order> getOrderListFromShelf(Shelf shelf) {
        List<Order> list = new ArrayList<>();
        for (Entry<String, Order> entry : shelf.getOrders()) {
            list.add(entry.getValue());
        }
        return list;
    }

    /**
     * Discard all shelves, and ignore given shelf and overflow.
     * The ignore logic is to avoid duplicate checking.
     */
    int discardShelvesExcept(Shelf except, String triggeringEvent) {
        int discardCount = 0;
        for (Shelf shelf : shelves) {
            if (shelf == except || shelf == Overflow) {
                continue;
            }
            discardCount += discardInShelf(shelf, triggeringEvent);
        }
        return discardCount;
    }

    /**
     * Discard any waste in given shelf.
     */
    int discardInShelf(Shelf shelf, String triggeringEvent) {
        // removing element using ArrayList's remove method during iteration
        // This will throw ConcurrentModification. so, use iterator.remove().
        // Note: shelf.getOrders() returns a clone of orders.
        int discardCount = 0;
        List<Order> list = getOrderListFromShelf(shelf);
        Iterator<Order> iter = list.listIterator();
        while (iter.hasNext()) {
            Order order = iter.next();
            if (order.isWasted()) {
                discardCount++;
                LOG.info("{} discarded waste in shelf {} {}", triggeringEvent, shelf, order);
                dispatcher.dispatch(new OrderDiscardedEvent(order));
                orderIdMap.remove(order.getId());
                iter.remove();
                showShelves();
            }
        }
        return discardCount;
    }


}
